package ru.kuzin.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.api.service.ITokenService;

@Getter
@Setter
public final class TokenService implements ITokenService {

    @Nullable
    private String token;

}